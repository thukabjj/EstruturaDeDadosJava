/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loiane.estruturadedados.vetorlabs;

import com.loiane.estruturadedados.vetor.Lista;

/**
 *
 * @author Codding
 */
public class Exer04 {
    public static void main(String[] args) {
        Lista <String> lista = new Lista<String>(5);
        lista.adiciona("A");
        lista.adiciona("B");
        lista.adiciona("A");
        System.out.println(lista.obtem(1));
    }
}
