/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loiane.estruturadedados.vetorlabs;

import com.loiane.estruturadedados.vetor.Lista;
import com.loiane.estruturadedados.vetor.teste.Contato;
import java.util.*;

/**
 *
 * @author Codding
 */
public class Exer06 {

    public static void main(String[] args) {

        //criação das variaveis
        Scanner scan = new Scanner(System.in);

        //criar vetor com 20 de capacidade
        Lista<Contato> lista = new Lista<Contato>(20);

        //criar e adcionar 30 contatos
        //criarContatosDinamicamente(5, lista);
        //criar um menu para que o usuario escolha a opcao
        int opcao = 1;
        while (opcao != 0) {
            opcao = obterOpcaoMenu(scan);

            switch (opcao) {
                case 1:
                    adicionarContatoFinal(scan, lista);
                    break;
                case 2:
                    adicionarContatoPosicao(scan, lista);
                case 3:
                    obtemContatoPosicao(scan, lista);
                    break;
                case 4:
                    obtemContato(scan, lista);
                    break;
                case 5:
                    pesquisarUltimoIndice(scan, lista);
                    break;
                case 6:
                    pesquisarContatoExiste(scan, lista);
                    break;
                case 7:
                    excluirPorPosicao(scan, lista);
                    break;
                case 8:
                    excluirContatoExiste(scan, lista);
                    break;
                case 9:
                    imprimeTamanhoVetor(lista);
                    break;
                case 10:
                    limparVetor(lista);
                    break;
                case 11:
                    imprimirVetor(lista);
                    break;
                default:
                    break;
            }
        }
        System.out.println("Usuário digitou 0, programa terminado!");

    }
    protected static void imprimirVetor(Lista<Contato> lista){
        System.out.println(lista);
    }
    protected static void limparVetor(Lista<Contato> lista){
        lista.limpar();
        System.out.println("Todos os contatos do vetor foram excluidos");
    }
    protected static void imprimeTamanhoVetor(Lista<Contato> lista){
        System.out.println("Tamanho do vetor é de: "+lista.tamanho());
    }
    protected static void excluirPorPosicao(Scanner scan, Lista<Contato> lista) {
        int pos = leInformacaoInt("Entre com a posição a ser exlcuida", scan);
        try {
            lista.remove(pos);
            System.out.println("Contato Excluído");

        } catch (Exception e) {
            System.out.println("Posição Inválida!");
        }
    }
     protected static void excluirContatoExiste(Scanner scan, Lista<Contato> lista) {
        int pos = leInformacaoInt("Entre com a posição a ser pesquisada", scan);
        try {
            Contato contato = lista.busca(pos);
            lista.remove(contato);

            System.out.println("Contato existe, seguem dados: ");
            System.out.println(contato);
            System.out.println("Contato excluido");
        } catch (Exception e) {
            System.out.println("Posição Inválida");
        }
    }
    protected static void pesquisarContatoExiste(Scanner scan, Lista<Contato> lista) {
        int pos = leInformacaoInt("Entre com a posição a ser pesquisada", scan);
        try {
            Contato contato = lista.busca(pos);

            
            boolean existe = lista.contem(contato);
            if (existe) {
                System.out.println("Contato existe, seguem dados: ");
                System.out.println(contato);
            } else {
                System.out.println("Contato não existe.");
            }

        } catch (Exception e) {
            System.out.println("Posição Inválida");
        }
    }

   

    protected static void pesquisarUltimoIndice(Scanner scan, Lista<Contato> lista) {
        int pos = leInformacaoInt("Entre com a posição a ser pesquisada", scan);
        try {
            Contato contato = lista.busca(pos);

            System.out.println("Contato existe, seguem dados: ");
            System.out.println(contato);

            System.out.println("Fazendo pesquisa do ultimo indice encontrado: ");
            pos = lista.ultimoIndice(contato);
            System.out.println("Contato encontrado na posição: " + pos);

        } catch (Exception e) {
            System.out.println("Posição Inválida");
        }
    }

    protected static void obtemContato(Scanner scan, Lista<Contato> lista) {
        int pos = leInformacaoInt("Entre com a posição a ser pesquisada", scan);
        try {
            Contato contato = lista.busca(pos);

            System.out.println("Contato existe, seguem dados: ");
            System.out.println(contato);

            System.out.println("Fazendo pesquisa do contato encontrado: ");
            pos = lista.busca(contato);
            System.out.println("Contato encontrado na posição: " + pos);

        } catch (Exception e) {
            System.out.println("Posição Inválida");
        }
    }

    protected static void obtemContatoPosicao(Scanner scan, Lista<Contato> lista) {
        int pos = leInformacaoInt("Entre com a posição a ser pesquisada", scan);
        try {
            Contato contato = lista.busca(pos);

            System.out.println("Contato existe, seguem dados: ");
            System.out.println(contato);
        } catch (Exception e) {
            System.out.println("Posição Inválida");
        }
    }

    protected static void adicionarContatoFinal(Scanner scan, Lista<Contato> lista) {
        System.out.println("Criando um contato, entre com as informações: ");
        String nome = leInformacao("Entre com o nome: ", scan);
        String telefone = leInformacao("Entre com o Telefone: ", scan);
        String email = leInformacao("Entre com o E-mail: ", scan);

        Contato contato = new Contato(nome, telefone, email);
        lista.adiciona(contato);

        System.out.println("Contato adicionado com sucesso");
        System.out.println(contato);
    }

    protected static void adicionarContatoPosicao(Scanner scan, Lista<Contato> lista) {
        System.out.println("Criando um contato, entre com as informações: ");
        String nome = leInformacao("Entre com o nome: ", scan);
        String telefone = leInformacao("Entre com o Telefone: ", scan);
        String email = leInformacao("Entre com o E-mail: ", scan);

        Contato contato = new Contato(nome, telefone, email);

        int pos = leInformacaoInt("Entre com a posição a adicionar o contato", scan);

        try {
            lista.adiciona(pos, contato);
            System.out.println("Contato adicionado com sucesso");
            System.out.println(contato);
        } catch (Exception e) {
            System.out.println("Posição inválida, contato não adicionado");
        }

    }

    protected static String leInformacao(String msg, Scanner scan) {
        System.out.println(msg);
        String entrada = scan.nextLine();
        return entrada;
    }

    protected static int leInformacaoInt(String msg, Scanner scan) {

        boolean entradaValida = false;
        int num = 0;

        while (!entradaValida) {
            try {
                System.out.println(msg);
                String entrada = scan.nextLine();
                
                num = Integer.parseInt(entrada);
                entradaValida = true;
                
               
            } catch (Exception e) {
                System.out.println("Entrada invalida digite novamente");
            }
        }
         return num;

    }

    protected static int obterOpcaoMenu(Scanner scan) {

        boolean entradaValida = false;
        int opcao = 0;
        String entrada;
        while (!entradaValida) {
            System.out.println("Digite a Opção desejada: \n");
            System.out.println("Opção 1: Adiciona contato no final do vetor");
            System.out.println("Opção 2: Adiciona contato em uma opção especifica do vetor");
            System.out.println("Opção 3: Obtem contato de uma posição especifica");
            System.out.println("Opção 4: Consulta contato");
            System.out.println("Opção 5: Consulta ultimo indice do contato");
            System.out.println("Opção  6: Verifica se o contato existe");
            System.out.println("Opção  7: Remove por Posição");
            System.out.println("Opção  8: Excluir Contato");
            System.out.println("Opção  9: Verifica tamanho do vetor");
            System.out.println("Opção  10: Excluir todos os contatos do vetor");
            System.out.println("Opção  11: Imprime vetor");
            System.out.println("Opção  0: Sair");

            try {
                entrada = scan.nextLine();
                opcao = Integer.parseInt(entrada);

                if (opcao >= 0 && opcao <= 11) {
                    entradaValida = true;
                } else {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("Entrada invalida digite a opção novamente \n\n");
            }

        }

        return opcao;
    }

    protected static void criarContatosDinamicamente(int quantidade, Lista<Contato> lista) {
        Contato contato;
        for (int i = 0; i < 20; i++) {
            contato = new Contato();
            contato.setNome("Contato" + i);
            contato.setTelefone("11111" + i);
            contato.setEmail("contato" + i + "@email.com");

            lista.adiciona(contato);

        }
    }
}
