/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loiane.estruturadedados.pilha;

import com.loiane.estruturadedados.base.EstruturaEstatica;

/**
 *
 * @author Codding
 */
public class Pilha<T> extends EstruturaEstatica {

    public Pilha(int capacidade){
        super(capacidade);
    }
    
    public void empilha(T elemento){
        super.adiciona(elemento);
    }
    public T topo(){
        if(this.estaVazio()){
            return null;
        }
        return (T) this.elementos[tamanho -1];
    }
}
