/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loiane.estruturadedados.pilha.teste;

import com.loiane.estruturadedados.pilha.Pilha;

/**
 *
 * @author Codding
 */
public class Aula15 {
    public static void main(String[] args) {
        Pilha<Integer> pilha = new Pilha<Integer>(10);
        System.out.println(pilha.estaVazio());
        
        pilha.empilha(1);
        System.out.println(pilha.estaVazio());

    }
}
