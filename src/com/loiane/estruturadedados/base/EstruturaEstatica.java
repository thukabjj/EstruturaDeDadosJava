/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.loiane.estruturadedados.base;

/**
 *
 * @author Codding
 */
public class EstruturaEstatica<T> {

    protected T[] elementos; //Aqui definimos que a classe tem um array do tipo Genérico, Definindo esse tipo de dado Através do Simbolo T que esta na declaração da classe.
    protected int tamanho; // Por fim o contador para dizer quantos espaços estão ocupados!

    public EstruturaEstatica(int capacidade) {//logo na aqui o construtor que recebe por parametro o tamanho do array do tipo generico 
        this.elementos = (T[]) new Object[capacidade]; //Solução do  Effective Java Second Edition
        this.tamanho = 0;
    }

    protected boolean adiciona(T elemento) {
        this.aumentaCapacidade();
        if (this.tamanho < this.elementos.length) {
            this.elementos[this.tamanho] = elemento;
            this.tamanho++;
            return true;
        }
        return false;
    }

    protected boolean adiciona(int posicao, T elemento) {
        this.aumentaCapacidade();
        if (!(posicao >= 0 && posicao < this.tamanho)) {
            throw new IllegalArgumentException("Posição inválida");
        }
        //mover todos os elementos
        for (int i = this.tamanho - 1; i >= posicao; i--) {
            this.elementos[i + 1] = this.elementos[i];
        }
        this.elementos[posicao] = elemento;
        this.tamanho++;
        return true;
    }

    private void aumentaCapacidade() {
        if (this.tamanho >= this.elementos.length) {
            T[] elementosNovos = (T[]) new Object[this.elementos.length * 2];
            for (int i = 0; i < this.elementos.length; i++) {
                elementosNovos[i] = this.elementos[i];
            }
            this.elementos = elementosNovos;
        }
    }
     public int tamanho() {
        return this.tamanho;
    }
     public boolean estaVazio(){
         return this.tamanho ==0;
     }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        s.append("[");
        for (int i = 0; i < this.tamanho - 1; i++) {
            s.append(this.elementos[i]);
            s.append(",");
        }
        if (this.tamanho > 0) {
            s.append(this.elementos[this.tamanho - 1]);
        }
        s.append("]");
        return s.toString();
    }
}
